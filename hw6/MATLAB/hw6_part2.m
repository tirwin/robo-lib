%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hw6_part2.m
% Trevor Irwin
%
% Analyze the data from the discrete time implementation of our filters in
% part 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clean up workspace
close all;
clear all;
clc;

% Read data
fid = fopen('data_part3_2.txt');
data = textscan(fid, '%f\t%f\t%f\t%f\t%f\n');
acc_data = data{1};
gyro_data = data{2};
lp_data = data{3};
hp_data = data{4};
sum_data = data{5};
n = .01*(length(acc_data)-1);
t = [0:.01:n];

% Plot data
plot(t, acc_data, 'r:');
hold on;
plot(t, gyro_data, 'g:');
plot(t, lp_data, 'r-');
plot(t, hp_data, 'g-');
plot(t, sum_data, 'b');
title('Filtered Theta');
xlabel('Time [s]');
ylabel('Theta [rad]');
legend('Raw Acc', 'Raw Gyro', 'LP Filter Acc', 'HP Filter Gyro', 'Filter Sum');
shg;