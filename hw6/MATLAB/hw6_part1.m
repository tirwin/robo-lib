%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hw6_part1.m
% Trevor Irwin
%
% Creates a first order low-pass and first order high-pass filter.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clean up workspace
close all;
clear all;
clc;

% Rise time selection
%
% Rise time for a first order filter is tr = ln(9)*tau;
% Therefore tau = tr/ln(9);
%
% The Mip takes less than .5 sec to fall over. In other words, the largest
% possible sudden motion will occur within .5 sec. If we select this as the
% rise time, we get tau = 0.23 and Wc = 4.39. This should still be a slow
% enough cut-off to catch most of the high frequency noise from the
% accelerometer. It will also hopefully be fast enough to remove the low
% frequency component of the gyros.
tr = 0.5;
tau = tr/log(9);
wc = 1/tau;
fprintf('Rise Time: %4.4f\nTau: %4.4f\nWc: %4.4f or 1/%4.2f\n', tr, tau, ...
    wc, tau);
%%
% Low & high pass filter coefficients
lpnum = [0, 1];
lpden = [tau, 1];
hpnum = [tau, 0];
hpden = [tau, 1];

% Create filters
lp = tf(lpnum, lpden);
hp = tf(hpnum, hpden);

% Plot
fig1 = figure();
bodeplot(lp ,'r');
hold on;
bodeplot(hp, 'g');
h = bodeplot(lp+hp, 'b');
setoptions(h, 'MagUnits', 'abs'); 
shg;

legend('Low Pass', 'High Pass', 'Sum');