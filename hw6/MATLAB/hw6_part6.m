function hw6_part6 ();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hw6_part6.m
% Trevor Irwin
%
% Evaluates the diophantine equation presented in HW6 Part 6 using the
% Euclidian and Reverse Euclidian methods.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference
%
%        b(s)   (s+2)(s-2)(s+4)(s-4)
% G(s) = ---- = ------------------------------
%        a(s)   (s+1)(s-1)(s+3)(s-3)(s+5)(s-5)
%
% b(s) = s^4 - 20 s^2 + 64
% a(s) = s^6 - 35 s^4 + 259 s^2 - 225
%
%        g(s)   b(s)*y(s)
% T(s) = ---- = ---------------------
%        f(s)   a(s)*x(s) + b(s)*y(s)
%
% Prescribe:
% F(s) = (s+1)^2(s+3)^2(s+5)^2      M.E.S.C
%                                   6th order
%
% Expanded: 
% f(s) = 
%   s^6 + 18 s^5 + 127 s^4 + 444 s^3 + 799 s^2 + 690 s + 225
%
% Begin with:
%
% f(s) is 6th order, a(s) is 6th order, so x(s) should be 0th order
% f(s) is 6th order, b(s) is 4th order, so y(s) should be 2nd order
%
%         y(s)          y2 s^2 + y1 s^1 + y0
% D(s)  = ---  = ---------------------------
%         x(s)                            x0
%
% However, F(s) has 7 terms and D(s) only has 4 DOF.
% Add 2 DOF to D(s)
%
% D(s)  = y(s)   y3 s^3 + y2 s^2 + y1 s^1 + y0
%         ---- = -----------------------------
%         x(s)                     x1 s^1 + x0
%
% Add one zero coefficients to f(s)
%
% f(s)  = 0s^7 + s^6 + 18 s^5 + 127 s^4 + 444 s^3 + 799 s^2 + 690 s + 225
%
% We now have that y(s)*b(s) = 7th order
% And we have that x(s)*a(s) = 7th order
% And we have that f(s) = 7th order, 8 terms
%
% D(s) has 6 terms, so we are still short 2 terms.
%
% Add two more to top and bottom of D(s), and two more terms to f(s)
%
% D(s) = y(s) = y5 s^5 + y4 s^4 + ...
%        ---    -----------------------
%        x(s)   x3 s^3 + x2 s^2 + ...
%
% f(s) = 0s^9 + 0s^8 + 0s^7 + s^6 + 18 s^5 + 127 s^4 + ...
%
% D(s) now has 10 terms
% f(s) now has 10 terms
% y(s)*b(s) = 9th order
% x(s)*a(s) = 9th order
%
% This checks out! Now we can solve!
%

% Clear workspace
close all;
clear all;
clc;

% Diophantine Solver
b = [1, 0, -20, 0, 64];
a = [1, 0, -35, 0, 259, 0, -225];
c = [1, 18, 127, 444, 799, 690, 225];
[x, y, r, s] = Diophantine(a,b,c);
roots(y)
roots(x)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results from MESC
%
% y(s) = (s-4.6370)(s+5)(s+3)(s-1.1609)(s+1)
% y(s) = 1.8222 s^5   5.8349 s^4  -43.3651 s^3 -127.3778 s^2 67.1429 s 147.1429
% y = [1.8222    5.8349  -43.3651 -127.3778   67.1429  147.1429]
% x(s) = (s-2.7999)(s+4.0004)(s+2.0016)
% x(s) =  -1.8222 s^3   -5.8349 s^2  16.0317 s^1  40.8540
% x = [-1.8222   -5.8349   16.0317   40.8540]
%
%
% THUS, OUR M.E.S.C IS:
%
%        (s-4.6370)(s+5)(s+3)(s-1.1609)(s+1)
% D(s) = -------------------------
%        (s-2.7999)(s+4.0004)(s+2.0016)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G = tf(b, a);
D1 = tf(y, x);
num = PolyConv(b, y);
den1 = PolyConv(a, x);
den2 = PolyConv(b, y);
den = PolyAdd(den1, den2);
T = tf(num, den);
figure();
bode(T);
title('Bode Plot of Closed-Loop System with MESC');
figure();
rlocus(G*D1);
title('Root Locus of MESC');
shg;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% From the bode plot, the crossover frequency is .657 rad/s
% Can't decide on a good degree of LPF. Going to use one OoM above largest
% pole, as this should also safely eliminate the high frequency
% amplification going on over 1
% 
% Prescribe New f(s):
%
% f(s) = (s+1)^2(s+3)^2(s+5)^2(s+50)^6      M.E.S.C needs to be
%                                           12th order
%
% Expanded: 
% f(s) = huge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

b = [1, 0, -20, 0, 64];
a = [1, 0, -35, 0, 259, 0, -225]; 
% %
% % (s+50)^2
% % This didn't work out for some reason. Kept getting improper results
% %
% c2 = [1 118 4427 58144 362699 1190590 2066725 1747500 562500]; %2nd?
%
% (s+10)^6
% More moderate root gives proper results
%
c2 = Poly([-1 -1 -3 -3 -5 -5 -10 -10 -10 -10 -10 -10]);
%

% Solve Diophantine
[x2, y2, r2, s2] = Diophantine(a, b, c2); 
roots(y2)
roots(x2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results from Diophantine with LPF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G = tf(b, a);
D2 = tf(y2, x2);
num = PolyConv(b, y2);
den1 = PolyConv(a, x2);
den2 = PolyConv(b, y2);
den = PolyAdd(den1, den2);
T2 = tf(num, den);
figure();
bode(T2);
title('Bode Plot of Closed-Loop Systen with MESC & LPF');
figure();
rlocus(G*D2);
title('Root Locus of MESC with Low Pass Filter');
shg;
clc;
fprintf('M.E.S.C Coefficients:\ny:');
fprintf('%.5g\t', y);
fprintf('\nx:');
fprintf('%.5g\t', x);
fprintf('\n');
fprintf('M.E.S.C w LPF Coefficients:\ny:');
fprintf('%.5g\t', y2);
fprintf('\nx:');
fprintf('%.5g\t', x2);
fprintf('\n');


end % function hw6_part6


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diophantine Code
% 
% Algorithm B1 from Numerical Renaissance (2016)
% Tom Bewley et al.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x,y,r,s] = Diophantine(a,b,c)
% function [x,y,r,s] = Diophantine(a,b,c)
% Solve the polynomial Diophantine eqn a*x+b*y=c via the Extended Euclidean algorithm
% for coprime {a,b}.  The solution {x,y} returned is the solution with the lowest order
% for y; the general solution is given by {x+r*t,y+s*t} for any polynomial t.
% See <a href="matlab:NRweb">Numerical Renaissance: simulation, optimization, & control</a>, Section B.2.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAB">Appendix B</a>; please read the <a href="matlab:help NRcopyleft">copyleft</a>.
% See DiophantineTest.

n=max(2,abs(length(a)-length(b))+1); rm=a; r=b; for i=1:n+2
  r=r(find(r,1):end); [quo,rem]=PolyDiv(rm,r);  % Reduce (rm,r) to their GCD via Euclid's
  q(i,n+1-length(quo):n)=quo; rm=r; r=rem;      % algorithm, saving the quotients quo.
if norm(r,inf)<1e-13, g=rm, break, end, end
r=-PolyDiv(b,g); s=PolyDiv(a,g); y=PolyDiv(c,g);
x=0;  for j=i-1:-1:1                            % Using q as computed above, compute {x,y}
  t=x; x=y; y=PolyAdd(t,-PolyConv(q(j,:),y));   % via the Extended Euclidean algorithm
end, y=y(find(y,1):end); [div,rem]=PolyDiv(y,s), t=-div   % Find the solution {x,y} that
x=PolyAdd(x,PolyConv(r,t)); x=x(find(abs(x)>1e-8,1):end); % minimizes the order of y; this
y=PolyAdd(y,PolyConv(s,t)); y=y(find(abs(y)>1e-8,1):end); % is the most useful in practice
end % function Diophantine

function [d,b]=PolyDiv(b,a)
% function [d,b]=PolyDiv(b,a)
% Perform polynomial division of a into b, resulting in d with remainder in the modified b.
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

m=length(b); n=length(a); if m<n d=0; else
  if strcmp(class(b),'sym')|strcmp(class(a),'sym'), syms d, end
  for j=1:m-n+1, d(j)=b(1)/a(1); b(1:n)=PolyAdd(b(1:n),-d(j)*a); b=b(2:end); end, end
end % function PolyDiv

function p=PolyConv(a,b,c,d,e,f,g,h,i,j)
% function p=PolyConv(a,b,c,d,e,f,g,h,i,j)
% Recursively compute the convolution of the two to ten polynomials, given as arguments.  
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

if nargin>9, a=PolyConv(a,j); end, if nargin>8, a=PolyConv(a,i); end
if nargin>7, a=PolyConv(a,h); end, if nargin>6, a=PolyConv(a,g); end
if nargin>5, a=PolyConv(a,f); end, if nargin>4, a=PolyConv(a,e); end
if nargin>3, a=PolyConv(a,d); end, if nargin>2, a=PolyConv(a,c); end
m=length(a); n=length(b); p=zeros(1,n+m-1);
for k=0:n-1; p=p+[zeros(1,n-1-k) b(n-k)*a zeros(1,k)]; end
end % function PolyConv

function a=PolyAdd(a,b,c,d,e,f,g,h,i,j)
% function a=PolyAdd(a,b,c,d,e,f,g,h,i,j)
% Add two to ten polynomials with right justification.
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

if nargin>9, a=PolyAdd(a,j); end, if nargin>8, a=PolyAdd(a,i); end
if nargin>7, a=PolyAdd(a,h); end, if nargin>6, a=PolyAdd(a,g); end
if nargin>5, a=PolyAdd(a,f); end, if nargin>4, a=PolyAdd(a,e); end
if nargin>3, a=PolyAdd(a,d); end, if nargin>2, a=PolyAdd(a,c); end
m=length(a); n=length(b); a=[zeros(1,n-m) a]+[zeros(1,m-n) b];
end % function PolyAdd

function p=Poly(r)
% function p=Poly(r)
% Compute the coefficients of the polynomial with roots r.
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

n=length(r); p=1; for i=1:n; p=PolyConv(p,[1 -r(i)]); end
end % function Poly