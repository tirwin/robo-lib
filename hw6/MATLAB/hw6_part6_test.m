function hw6_part6_test()

close all;
clear all;
clc;

b = [1 0 -4];
a = [1 0 -10 0 9];
c = Poly([-1 -1 -3 -3 -30 -30 -30 -30]);
[x, y, r, s] = Diophantine(a,b,c);
clc;
fprintf('M.E.S.C Coefficients:\ny:');
fprintf('%.5g\t', y);
fprintf('\nx:');
fprintf('%.5g\t', x);
fprintf('\n');

G = tf(b, a);
D1 = tf(y, x);
num = PolyConv(b, y);
den1 = PolyConv(a, x);
den2 = PolyConv(b, y);
den = PolyAdd(den1, den2);
T = tf(num, den);
figure();
bode(T);
title('Bode Plot of Closed-Loop System with MESC');
figure();
rlocus(G*D1);
title('Root Locus of MESC');
shg;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diophantine Code
% 
% Algorithm B1 from Numerical Renaissance (2016)
% Tom Bewley et al.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x,y,r,s] = Diophantine(a,b,c)
% function [x,y,r,s] = Diophantine(a,b,c)
% Solve the polynomial Diophantine eqn a*x+b*y=c via the Extended Euclidean algorithm
% for coprime {a,b}.  The solution {x,y} returned is the solution with the lowest order
% for y; the general solution is given by {x+r*t,y+s*t} for any polynomial t.
% See <a href="matlab:NRweb">Numerical Renaissance: simulation, optimization, & control</a>, Section B.2.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAB">Appendix B</a>; please read the <a href="matlab:help NRcopyleft">copyleft</a>.
% See DiophantineTest.

n=max(2,abs(length(a)-length(b))+1); rm=a; r=b; for i=1:n+2
  r=r(find(r,1):end); [quo,rem]=PolyDiv(rm,r);  % Reduce (rm,r) to their GCD via Euclid's
  q(i,n+1-length(quo):n)=quo; rm=r; r=rem;      % algorithm, saving the quotients quo.
if norm(r,inf)<1e-13, g=rm, break, end, end
r=-PolyDiv(b,g); s=PolyDiv(a,g); y=PolyDiv(c,g);
x=0;  for j=i-1:-1:1                            % Using q as computed above, compute {x,y}
  t=x; x=y; y=PolyAdd(t,-PolyConv(q(j,:),y));   % via the Extended Euclidean algorithm
end, y=y(find(y,1):end); [div,rem]=PolyDiv(y,s), t=-div   % Find the solution {x,y} that
x=PolyAdd(x,PolyConv(r,t)); x=x(find(abs(x)>1e-8,1):end); % minimizes the order of y; this
y=PolyAdd(y,PolyConv(s,t)); y=y(find(abs(y)>1e-8,1):end); % is the most useful in practice
end % function Diophantine

function [d,b]=PolyDiv(b,a)
% function [d,b]=PolyDiv(b,a)
% Perform polynomial division of a into b, resulting in d with remainder in the modified b.
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

m=length(b); n=length(a); if m<n d=0; else
  if strcmp(class(b),'sym')|strcmp(class(a),'sym'), syms d, end
  for j=1:m-n+1, d(j)=b(1)/a(1); b(1:n)=PolyAdd(b(1:n),-d(j)*a); b=b(2:end); end, end
end % function PolyDiv

function p=PolyConv(a,b,c,d,e,f,g,h,i,j)
% function p=PolyConv(a,b,c,d,e,f,g,h,i,j)
% Recursively compute the convolution of the two to ten polynomials, given as arguments.  
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

if nargin>9, a=PolyConv(a,j); end, if nargin>8, a=PolyConv(a,i); end
if nargin>7, a=PolyConv(a,h); end, if nargin>6, a=PolyConv(a,g); end
if nargin>5, a=PolyConv(a,f); end, if nargin>4, a=PolyConv(a,e); end
if nargin>3, a=PolyConv(a,d); end, if nargin>2, a=PolyConv(a,c); end
m=length(a); n=length(b); p=zeros(1,n+m-1);
for k=0:n-1; p=p+[zeros(1,n-1-k) b(n-k)*a zeros(1,k)]; end
end % function PolyConv

function a=PolyAdd(a,b,c,d,e,f,g,h,i,j)
% function a=PolyAdd(a,b,c,d,e,f,g,h,i,j)
% Add two to ten polynomials with right justification.
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

if nargin>9, a=PolyAdd(a,j); end, if nargin>8, a=PolyAdd(a,i); end
if nargin>7, a=PolyAdd(a,h); end, if nargin>6, a=PolyAdd(a,g); end
if nargin>5, a=PolyAdd(a,f); end, if nargin>4, a=PolyAdd(a,e); end
if nargin>3, a=PolyAdd(a,d); end, if nargin>2, a=PolyAdd(a,c); end
m=length(a); n=length(b); a=[zeros(1,n-m) a]+[zeros(1,m-n) b];
end % function PolyAdd

function p=Poly(r)
% function p=Poly(r)
% Compute the coefficients of the polynomial with roots r.
% See <a href="matlab:NRweb">Numerical Renaissance</a>, Appendix A, for further discussion.
% Part of <a href="matlab:help NRC">Numerical Renaissance Codebase 1.0</a>, <a href="matlab:help NRchapAA">Appendix A</a>; see webpage for <a href="matlab:help NRcopyleft">copyleft info</a>.                                                                   

n=length(r); p=1; for i=1:n; p=PolyConv(p,[1 -r(i)]); end
end % function Poly
