# ROBO-LIB

A collection of libraries and programs for my MIP project in MAE 144.

Homework 7/Final Project files are located in these directories:
* final_project/
* final_project/Report/
* lib/


Homework 6 files are located in these directories:
* hw6/
* hw6/MATLAB
* lib/



---

## Reminders to self

### Aliases

Use `bbssh` to SSH into the BeagleBone.
Use `bbcd` to navigate to the relevant directory (e.g. final_project/)
Use `bbmake` to clean, uninstall, clean again, and install
Use `bbmnt` to mount the BeagleBone's filesystem over SSH.
Use `bbumnt` to unmount the BeagleBone's filesystem.