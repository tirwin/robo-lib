%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot_my_read_sensors.m
% Plot the data output from my_read_sensors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fid = fopen('data.txt');
data = textscan(fid, '%f\t%f\n');
acc_data = data{1};
gyro_data = data{2};
n = .05*(length(acc_data)-1);
t = [0:.05:n];

plot(t, acc_data);
hold on;
plot(t, gyro_data);
title('Measurements of Theta');
xlabel('Time [s]');
ylabel('Theta [rad]');
legend('Accelerometer', 'Gyroscopes');