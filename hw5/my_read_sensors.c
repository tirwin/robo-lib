/*******************************************************************************
* my_read_sensors.c
*
* Read the accelerometer and gyroscopes and output the angle between the 
* BeagleBone in upright orientation and gravity.
* Output this data continuously to new lines when the "Mode" button is pressed.
* Re-align the gyro angle with the accelerometer angle when "Pause" is pressed.
*******************************************************************************/

#include <usefulincludes.h>
#include <roboticscape.h>


// function declarations
int print_accel_data();
int print_accel_setup();
int on_mode_released();
int on_pause_released();

// global variables (file scope)
static imu_data_t my_data;
static int verbose_output = 0;
static int gyro_initialized = 0;
static int imu_sample_rate = 20;
static float acc_theta = -1;
static float gyro_theta = -1;
static float time_step = 0.05;


/*******************************************************************************
* int main() 
*
* Initialize cape, run main loop, and clean up cape	
*******************************************************************************/
int main() {
	// Initialize the cape library
	initialize_cape();

	// Welcome message
	printf("\nMy Read Sensors\n");
	
	// Bind physical button interrupts
	set_mode_released_func(&on_mode_released);
	set_pause_released_func(&on_pause_released);

	// done initializing so set state to RUNNING
	set_state(RUNNING); 
	
	// Set up the accelerometer and console for printing
	print_accel_setup();

	// Keep looping until state changes to EXITING
	while(get_state()!=EXITING){
		
		// handle other states
		if(get_state()==RUNNING){
			// do things
			set_led(GREEN, ON);
		}
		// always sleep at some point
		usleep(50000);
	}
	
	// exit cleanly
	cleanup_cape(); 
	return 0;
}

/*******************************************************************************
* int print_accel_setup()
*	
* Configure the IMU for sampling at defined rate, set up the IMU interrupt
* functions,and ensure the console is set up for printing
*******************************************************************************/
int print_accel_setup(){
	int err = 0;
	
	// Get default imu config
	imu_config_t conf = get_default_imu_config();
	
	// Configure imu as per assignment instructions
	conf.dmp_sample_rate = imu_sample_rate;
	
	// Initialize IMU
	err = initialize_imu_dmp(&my_data, conf);
	if (err) { printf("ERROR: Failed to initialize imu"); return -1; }
	
	// Bind the interrupt function
	err = set_imu_interrupt_func(&print_accel_data);
	if (err) { printf("ERROR: Failed to bind IMU interrupt"); return -1; }
	
	// If successful, let us know!
	printf("Successfully configured IMU");
	
	// Ensure we have clean lines for writing to console
	printf("\n\n\n");
	
	return 0;
}

/*******************************************************************************
* int print_accel_data()
*	
* Print the accelerometer data to the console. This function is intended to be
* bound to the IMU interrupt
*******************************************************************************/
int print_accel_data(){ 

	// Calculate the angle of the BeagleBone using the accelerometer
	acc_theta = atan2(-my_data.accel[2], my_data.accel[1]);
	
	// Since gyro_theta is the result of integration, it requires an initial
	// condition. Check if we have "initialized" gyro_theta by setting this
	// initial condition, and if not then equate gyro_theta to acc_theta.
	if (!gyro_initialized) {
		gyro_theta = acc_theta;
		gyro_initialized = 1;
	}
	
	// Calculate the angle of the BeagleBone by integrating the gyros
	gyro_theta += my_data.gyro[0]*PI/180*time_step;
	
	// Print IMU data and flush buffer
	if (!verbose_output) {
		printf("\033[2A"); // Go up 2 lines
		printf("\033[K"); 	// Clear the line
		printf("Acc Theta: %3.2f [rad]\n", acc_theta); //Print acc theta
		printf("\033[K"); 	// Clear the line
		printf("Gyro Theta: %3.2f [rad]\n", gyro_theta); //Print gyro theta
		printf("\033[K"); 	// Clear the line
		printf("X: %5.1f | Y: %5.1f | Z: %5.1f [m/s^2]", my_data.accel[0], 
			my_data.accel[1], my_data.accel[2]); //Print xyz
		printf("\r"); //Return the cursor to beginning of the line
		fflush(stdout);
	} else {
		printf("%7.5f\t%7.5f\n", acc_theta, gyro_theta); //Print theta and gyro
	}
	return 0;
	
}

/*******************************************************************************
* int on_pause_released()
*	
* Eliminate accumulated error in gyro_theta by equating to acc_theta
*******************************************************************************/
int on_pause_released(){
	gyro_theta = acc_theta;
	return 0;
}

/*******************************************************************************
* int on_mode_released()
*	
* Toggle between verbose and compact accelerometer output
* Verbose output prints every line of data to a new line in the console
* Compact output prints to the same line in the console
*******************************************************************************/
int on_mode_released(){
	verbose_output = !verbose_output;
	set_led(RED, verbose_output);
	return 0;
}