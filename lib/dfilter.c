/*******************************************************************************
* dfilter.c
* Trevor Irwin 2016
*
* This is a simple library for creating discrete causal or strictly causal filter
* with polynomial numerators and denominators
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

#include "circular_buffer.h"

/*******************************************************************************
* typedef dfilter_t
* A struct typedef for the circular buffer
* This buffer allows for efficiently "shifting" values every time a new value is
* added, without rewriting each address in memory
*******************************************************************************/
typedef struct dfilter_t {
	int order;			//Order of highest degree polynomial
	float* num;			//Coefficients of numerator
	float* den;			//Coefficients of denominator
	cbuff_t* input;		//Output buffer
	cbuff_t* output;	//Input buffer
	float sat_on;
	float sat_enforce;
	float sat_upper_lim;
	float sat_lower_lim;
	float is_saturated;
	float gain;
} dfilter_t;

/*******************************************************************************
* create_dfilter
* Create a dfilter struct, allocating memory for its respective components
*
* Args: order is the order of the numerator or denominator, whichever is higher
		num is an array containing numerator coefficients
		den is an array containing denominator coefficients
* Returns: a pointer to the dfilter
*******************************************************************************/
dfilter_t* create_dfilter(int order, float num[], float den[]) {
	dfilter_t* dfilter = (dfilter_t*)malloc(sizeof(dfilter_t));
	dfilter->order = order;
	dfilter->num = (float*)calloc(order+1, sizeof(float));
	dfilter->den = (float*)calloc(order+1, sizeof(float));
	dfilter->input = create_circular_buffer(order+1);
	dfilter->output = create_circular_buffer(order+1);
	dfilter->sat_on = 0;
	dfilter->sat_enforce = 0;
	dfilter->sat_upper_lim = 1;
	dfilter->sat_lower_lim = -1;
	dfilter->is_saturated = 0;
	dfilter->gain = 1;
	
	int i = 0;
	for (i = 0; i <= order; ++i) {
		dfilter->num[i] = num[i];
		dfilter->den[i] = den[i];
	}
	return dfilter;
}

/*******************************************************************************
* advance_dfilter
* Advance the dfilter by one time step
*
* Args: dfilter is a pointer to the dfilter struct in question
*		val is the new input to the filter
*
* Returns: The latest output to the filter
*******************************************************************************/
float advance_dfilter(dfilter_t* dfilter, float val) {
	
	// Add the latest input value to the buffer
	insert_circular_buffer(dfilter->input, val);
	
	// Some initialization and convenience variables
	float new_u = 0;
	int order = dfilter->order;
	int i = 0;

	// Calculate value of new_u with numerator coefficients/input
	for (i = 0; i <= order; ++i) {
		float input = read_circular_buffer(dfilter->input, i);
		float coeff = dfilter->num[i];
		new_u += input * coeff * dfilter->gain;;
	}
	
	// Add to value of new_u with denominator coefficients/output
	for (i = 1; i <= order; ++i) {
		float output = read_circular_buffer(dfilter->output, i-1);
		float coeff = dfilter->den[i];
		new_u -= output * coeff;
	}
	// Scale new output based on first term in denominator
	new_u = new_u/(dfilter->den[0]);

	// Check saturation
	if (dfilter->sat_on) {
		if (new_u > dfilter->sat_upper_lim) {
			if (dfilter->sat_enforce) {
				new_u = dfilter->sat_upper_lim;
			}
			dfilter->is_saturated = 1;
		} else if (new_u < dfilter->sat_lower_lim) {
			if (dfilter->sat_enforce) {
				new_u = dfilter->sat_lower_lim;
			}
			dfilter->is_saturated = 1;
		} else {
			dfilter->is_saturated = 0;
		}
	}

	// Add the new output to the output buffer
	insert_circular_buffer(dfilter->output, new_u);

	
	return new_u;
}

/*******************************************************************************
* destroy_dfilter
* De-allocate memory for a dfilter
*
* Args: dfilter is a pointer to the discrete filter struct in question
*
* Returns: 0 for success, -1 for failure
*******************************************************************************/
int destroy_dfilter(dfilter_t* dfilter) {
	//Check if the filter has already been destroyed
	if (!dfilter) {
		printf("ERROR: Dfilter is NULL. Was it already destroyed?");
		return -1;
	}
	// Destroy the input and output buffers
	int err = 0;
	err += destroy_circular_buffer(dfilter->input);
	err += destroy_circular_buffer(dfilter->output);
	if (err) {
		printf("ERROR: Failed to destroy dfilter buffers.");
	}
	// Free numerator and denominator
	free(dfilter->num);
	free(dfilter->den);
	return 0;
}



/*******************************************************************************
* preload_dfilter
* Pre-load a filter's buffer with a given value.
* Prevents spikes in e.g. high pass filter output
*
* Args: dfilter is a pointer to the discrete filter struct in question
*
* Returns: 0 for success, -1 for failure
*******************************************************************************/
int preload_dfilter(dfilter_t* dfilter, float val) {
	if (!dfilter) return -1;
	int order = dfilter->order;
	int i = 0;
	for (i = 0; i <= order; ++i) {
		insert_circular_buffer(dfilter->input, val);
		insert_circular_buffer(dfilter->output, val);
	}
	return 0;
}

/*******************************************************************************
* set_dfilter_sat_limits
* Set the filter's saturation limits, which determine whether is_saturated is
* returned true or false
*
* Args: dfilter is a pointer to the discrete filter struct in question
*		low_lim is the lower saturation limit
*		high_lim is the upper saturation limit
*
* Returns: 0 for success, -1 for failure
*******************************************************************************/

int set_dfilter_sat_limits(dfilter_t* dfilter, float low_lim, float high_lim){
	if (!dfilter) return -1;
	if (low_lim == high_lim) {
		dfilter->sat_on = 0;
	} else {		
		dfilter->sat_upper_lim = high_lim;
		dfilter->sat_lower_lim = low_lim;
		dfilter->sat_on = 1;
	}
	return 0;
}

/*******************************************************************************
* set_dfilter_sat_enforce
* Set whether the filter should clamp its output to its saturation limits.
*
* Args: dfilter is a pointer to the discrete filter struct in question.
*		should_enforce is an int where 1 is true and 0 is false
*
* Returns: 0 for success, -1 for failure
*******************************************************************************/

int set_dfilter_sat_enforce(dfilter_t* dfilter, int should_enforce){
	if (!dfilter) return -1;
	dfilter->sat_enforce = should_enforce;
	return 0;
}

/*******************************************************************************
* int set_dfilter_gain(dfilter_t* dfilter, float gain)
* Set the dfilter's gain, which scales all of the terms in the numerator.
*
* Args: dfilter is a pointer to the discrete filter struct in question.
*		gain is the value by which to scale the numerator coefficients
*
* Returns: 0 for success, -1 for failure
*******************************************************************************/

int set_dfilter_gain(dfilter_t* dfilter, float gain){
	if (!dfilter) return -1;
	dfilter->gain = gain;
	return 0;
}

/*******************************************************************************
* dfilter_is_saturated
* Return whether the dfilter is saturated or not
*
* Args: dfilter is a pointer to the discrete filter struct in question.
*
* Returns: 1 if saturated, 0 if not, -1 if an error occurs
*******************************************************************************/

int dfilter_is_saturated(dfilter_t* dfilter){
	if (!dfilter) return -1;
	return dfilter->is_saturated;
}

/*******************************************************************************
* int print_dfilter(dfilter_t* dfilter)
* Print the coefficients of the filter to the console.
*
* Extremely crude and intendedstrictly for debugging.
*
* Args: dfilter is a pointer to the discrete filter struct in question.
*
* Returns: 0 for success
*******************************************************************************/

int print_dfilter(dfilter_t* dfilter){
	int i = 0;
	printf("\n");
	for (i=0;i<=dfilter->order;++i){
		printf("%+-5.3f\t", dfilter->num[i]);
	}
	for (i=0;i<=dfilter->order;++i){
		printf("%+-5.3f\t", dfilter->den[i]);
	}
	return 0;
}


/*******************************************************************************
* Test Code
*******************************************************************************/

// int main() {
// 	printf("Testing dfilter\n");
// 	
// 	float c = 0.01;
// 	
// 	int lporder = 1;
// 	float lpnum[2] = {c, 0};
// 	float lpden[2] = {1, c-1};
// 	
// 	dfilter_t* lpfilter = create_dfilter(lporder, lpnum, lpden);
// 	
// 	int hporder = 1;
// 	float hpnum[2] = {1-c, c-1};
// 	float hpden[2] = {1, c-1};
// 	
// 	
// 	dfilter_t* hpfilter = create_dfilter(hporder, hpnum, hpden);
// 	//preload_filter(hpfilter, 10);
// 	
// 	int i = 0;
// 	int j = 1;
// 
// 	float lpval = 0;
// 	float hpval = 0;
// 	printf("\n\n\n\n\n");
// 	while (j > 0) {
// 		while (i < 500) {
// 			lpval = advance_dfilter(lpfilter, j*1.0);
// 			hpval = advance_dfilter(hpfilter, j*1.0);
// 
// 
// 			printf("\033[2A"); // Go up 2 lines
// 			printf("\r"); //Return to beginning of line
// 			printf("\033[K"); // Clear the line
// 			printf("J: %d\n", j*1);
// 			printf("\033[K"); // Clear the line
// 			printf("Index: %d\n", i);
// 			printf("\033[K"); // Clear the line
// 			printf("LP Value: %f       HP Value: %f", lpval, hpval);
// 
// // 			printf("i: %d     LP Value: %f       HP Value: %f\n", i, lpval, hpval);
// 			fflush(stdout);
// 			
// 			
// 			usleep(10000); 
// 			++i;
// 		}
// 		i = 0;
// 		++j;
// 	}
// 	
// 	return 0;
// }
