/*******************************************************************************
* circular_buffer.c
* Trevor Irwin 2016
*
* This is a simple library implementing the circular buffer
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*******************************************************************************
* typedef cbuff_t
* A struct typedef for the circular buffer
* This buffer allows for efficiently "shifting" values every time a new value is
* added, without rewriting each address in memory
*******************************************************************************/
typedef struct cbuff_t {
	int size;		//Number of elements in the buffer
	int pos;		//Current position of the buffer
	float* data;	//Contents of the buffer
} cbuff_t;

/*******************************************************************************
* create_circular_buffer
* Initialize a circular buffer and allocate memory for it
*
* Args: Size (number of elements to include in the buffer)
* Returns: The buffer as type cbuff_t
*******************************************************************************/
cbuff_t* create_circular_buffer(int size) {
	cbuff_t* cbuff = (cbuff_t*)malloc(sizeof(cbuff_t));
	cbuff->size = size;
	cbuff->pos = 0;
	cbuff->data = (float*)calloc(size, sizeof(float));
	return cbuff;
}

/*******************************************************************************
* insert_circular_buffer
* Insert a new value into the circular buffer
*
* Args: cbuff - the circular buffer
*		val - the value to insert
* Returns: 0 for success, -1 for failure
*******************************************************************************/
int insert_circular_buffer(cbuff_t* cbuff, float val) {
	// Check if the buffer data is valid
	if (!cbuff) {
		printf("ERROR: cbuff is NULL!");
		return -1;
	}
	// Get the current buffer position because we will want to modify it
	int pos = cbuff->pos;
	pos--;
	// Cap the position if it goes over the max size
	if (pos < 0) {
		pos = cbuff->size -1;
	}
	//Write the new pos
	cbuff->pos = pos;
	// Set the data at the current position to the new value
	cbuff->data[pos] = val;
	return 0;
}

/*******************************************************************************
* read_circular_buffer
* Read a value from the circular buffer.
*
* Args: cbuff is a pointer to the circular buffer.
* 		offset is a positive, zero-based offset - an offset of 2 will read the third
* 		most recent value entered into the buffer.
*
* Returns: The value of the buffer at the provided offset OR -1 for failure
* 			MUST BE VERY CAUTIOUS WITH ERROR CHECKING IF -1 IS A VALUE THAT MAY BE
* 			CONTAINED IN THE BUFFER!
*******************************************************************************/
float read_circular_buffer(cbuff_t* cbuff, int offset) {
	// Check if the buffer data is valid
	if (!cbuff) {
		printf("ERROR: cbuff is NULL!");
		return -1;
	}
	// Wrap the position around, then return the size
	int pos = (cbuff->pos + offset) % (cbuff->size);
	return cbuff->data[pos];
}

/*******************************************************************************
* destroy_circular_buffer
* Frees the memory used by the circular buffer
*
* Args: cbuff is a pointer to the circular buffer
*
* Returns: 0 for success, -1 for failure
*******************************************************************************/
int destroy_circular_buffer(cbuff_t* cbuff) {
	// Check if the buffer has already been destroyed
	if (!cbuff) {
		printf("ERROR: cbuff is NULL!");
		return -1;
	}
	free(cbuff->data);
	free(cbuff);
	return 0;
}


/*******************************************************************************
* Test Code
*******************************************************************************/

// int main() {
// 	printf("Hello world!\n");
// 	cbuff_t* mybuff = create_circular_buffer(2);
// 
// 	int i;
// 	for (i = 0; i < 95; ++i) {
// 		insert_circular_buffer(mybuff, (float)i);
// 		printf("Inserted: %d\n", i);
// 	}
// 	for (i = 0; i < 2; ++i) {
// 		float var = read_circular_buffer(mybuff, i);
// 		printf("Buffer read: %f\n", var);
// 	}
// 	destroy_circular_buffer(mybuff);
// 	mybuff = NULL;
// 	return 0;
// }