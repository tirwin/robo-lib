/*******************************************************************************
* circular_buffer.h
* Trevor Irwin 2016
*
* This is a simple library implementing the circular buffer
*******************************************************************************/

#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

/*******************************************************************************
* typedef struct cbuff_t
* The actual circular buffer struct. This is an easy way to maintain a buffer of 
* "shifting" values without actually rewriting each address in memory.
* Declared as an opaque pointer to prevent inappropriate usage.
*******************************************************************************/
typedef struct cbuff_t cbuff_t;

/*******************************************************************************
* @ cbuff_t* create_circular_buffer(int size);
*
* Create and allocate memory for a circular buffer struct of the given size
*
* @ int insert_circular_buffer(cbuff_t* cbuff, float val);
*
* Add a value to the newest position in the circular buffer.
*
* @ float read_circular_buffer(cbuff_t* cbuff, int offset);
*
* Read the last value off the circular buffer, or read a value offset by the
* provided number of spaces.
*
* @ int destroy_circular_buffer(cbuff_t* cbuff);
*
* Destroy the circular buffer struct, freeing up memory.
*
*******************************************************************************/
cbuff_t* create_circular_buffer(int size);
int insert_circular_buffer(cbuff_t* cbuff, float val);
float read_circular_buffer(cbuff_t* cbuff, int offset);
int destroy_circular_buffer(cbuff_t* cbuff);

#endif /*  CIRCULAR_BUFFER_H  */