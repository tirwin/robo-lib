/*******************************************************************************
* dfilter.h
* Trevor Irwin 2016
*
* This is a simple library for creating discrete causal or strictly causal filter
* with polynomial numerators and denominators
*******************************************************************************/

#ifndef DFILTER_H
#define DFILTER_H

/*******************************************************************************
* @ typedef dfilter_t
* A struct typedef for the discrete time filter. This is an opaque
* implementation, so the struct's contents must be accessed by other functions
* in this library
*******************************************************************************/
typedef struct dfilter_t dfilter_t;

/*******************************************************************************
* @ dfilter_t* create_dfilter(int order, float num[], float den[])
*
* Create a discrete-time SISO filter with the provided numerator and denominator coefficients.
*
* @ float advance_dfilter(dfilter_t* dfilter, float val)
*
* March the discrete filter one timestep using the provided input.
*
* @ int destroy_dfilter(dfilter_t* dfilter);
*
* Destroy a dfilter struct, freeing up memory
*
* @ int preload_dfilter(dfilter_t* dfilter, float val)
*
* Fill a dfilter's input and output buffer's with a constant series of data.
*
* @ int set_dfilter_sat_limits(dfilter_t* dfilter, float low_lim, float high_lim);
*
* Set the upper and lower saturation limits for this filter. Saturation limits
* determine when the dfilter_is_saturated() function returns true. They can also
* be used with set_dfilter_sat_enforce to limit the values of the output of the
* filter to the min and max saturation limits.
* 
* @int dfilter_is_saturated(dfilter_t* dfilter)
*
* Returns 1 when the dfilter is saturated and 0 otherwise.
*
* @ int set_dfilter_gain(dfilter_t* dfilter, float gain)
*
* Sets the dfilter's gain, which scales all of the terms in the numerator.
*
* @ int print_dfilter(dfilter_t* dfilter)
*
* Print the coefficients of the dfilter. A sloppy function strictly for
* debugging.
*
*******************************************************************************/
dfilter_t* create_dfilter(int order, float num[], float den[]);
float advance_dfilter(dfilter_t* dfilter, float val);
int destroy_dfilter(dfilter_t* dfilter);
int preload_dfilter(dfilter_t* dfilter, float val);
int set_dfilter_sat_limits(dfilter_t* dfilter, float low_lim, float high_lim);
int set_dfilter_sat_enforce(dfilter_t* dfilter, int should_enforce);
int dfilter_is_saturated(dfilter_t* dfilter);
int set_dfilter_gain(dfilter_t* dfilter, float gain);
int print_dfilter(dfilter_t* dfilter);


#endif /*  DFILTER_H  */