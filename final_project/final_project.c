/*******************************************************************************
* final_project.c
* Trevor Irwin 2016
*
* Implementation of a mobile inverted pendulum balance controller for
* the MAE 144 final project.
*******************************************************************************/

#include <usefulincludes.h>
#include <roboticscape.h>
#include "../lib/dfilter.h"
#include "final_config.h"


// function declarations
int drive_setup();
int imu_process();
int on_mode_released();
int on_pause_released();
int disarm_controllers();
void* d1_control(void* ptr);
void* d2_control(void* ptr);
void* d3_control(void* ptr);
void* read_encoders(void* ptr);
void* voltage_monitor(void* ptr);
void* manage_setpoint(void* ptr);
void* print_data(void* ptr);


// Setpoint definition
typedef struct setpoint_t {
	float theta;
	float phi;
	float gamma;
	
	enum arm_state_t arm_state;
} setpoint_t;

// State definition
typedef struct mipstate_t {
	float acc;
	float gyro;
	float lp;
	float hp;
	
	float wheelL;
	float wheelR;
	
	float theta;
	float phi;
	float gamma;
	
	float err_theta;
	float err_phi;
	float err_gamma;
	
	float d1_output;
	float d2_output;
	float d3_output;
	
	float motor_output_L;
	float motor_output_R;
	
	float v_battery;
} mipstate_t;


// Global variables (file scope)
static setpoint_t setpoint = {0};
static mipstate_t mipstate = {0};
static imu_data_t imu_data;
static int gyro_initialized = 0;
static int verbose_output = 1;


// Filters
static dfilter_t* lp_filter;
static dfilter_t* hp_filter;
static dfilter_t* d1_filter;
static dfilter_t* d2_filter;
static dfilter_t* d3_filter;


/*******************************************************************************
* int main() 
*
* Initialize cape, set up button functions and IMU, create threads
* Run main loop
* Cleanly clean up program and cape on exit
*******************************************************************************/
int main() {
	// CPU Freq
	set_cpu_frequency(FREQ_1000MHZ);
	
	// Initialize the cape library
	initialize_cape();

	// Welcome message
	printf("\nMiP Balance Program\n");
	
	// Bind physical button interrupts
	set_mode_released_func(&on_mode_released);
	set_pause_released_func(&on_pause_released);
	
	// Set up the the IMU and create filters
	drive_setup();
	
	// Allocate threads
	pthread_t encoder_thread;
	pthread_t print_thread;
	pthread_t d1_thread;
	pthread_t d2_thread;	
	pthread_t d3_thread;
	pthread_t voltage_thread;
	pthread_t setpoint_thread;
	
	// Couldn't get priority setting working
	// in a reasonable amount of time
	//
	// Configure thread priority
	// struct sched_param d1_params;
	// struct sched_param d2_params;
	// struct sched_param d3_params;
	// int max_priority = sched_get_priority_max(SCHED_FIFO);
	// d1_params.sched_priority = max_priority - D1_PRIORITY;
	// d2_params.sched_priority = max_priority - D2_PRIORITY;
	// d3_params.sched_priority = max_priority - D3_PRIORITY;
	// pthread_setschedparam(d1_thread, SCHED_FIFO, &d1_params);
	// pthread_setschedparam(d2_thread, SCHED_FIFO, &d2_params);
	// pthread_setschedparam(d3_thread, SCHED_FIFO, &d3_params);
	
	// Create threads
	pthread_create(&encoder_thread, NULL, read_encoders, (void*) NULL);
	pthread_create(&print_thread, NULL, print_data, (void*) NULL);
	pthread_create(&d1_thread, NULL, d1_control, (void*) NULL);
	pthread_create(&d2_thread, NULL, d2_control, (void*) NULL);
	pthread_create(&d3_thread, NULL, d3_control, (void*) NULL);
	pthread_create(&voltage_thread, NULL, &voltage_monitor, (void*) NULL);
	pthread_create(&setpoint_thread, NULL, &manage_setpoint, (void*) NULL);
	
	// done initializing so set state to RUNNING
	set_state(PAUSED); 

	// Keep looping until state changes to EXITING
	while(get_state()!=EXITING){
		
		// Set LEDs to indicate the current state
		// RED light indicates the MiP is paused
		// GREEN light indicates the controllers are armed and running
		if(get_state()==RUNNING){
			set_led(RED, OFF);
		} else {
			set_led(RED, ON);
			disarm_controllers();
		}
		if (setpoint.arm_state == ARMED) {
			set_led(GREEN, ON);
		} else {
			set_led(GREEN, OFF);
		}
		// always sleep at some point
		usleep(100000); //tenth second
	}
	
	// Ensure controllers start disarmed
	disarm_controllers();
	
	// Couldn't get priority setting working
	// in a reasonable amount of time
	//
	// State is now EXITING, clean up other threads
	// timespec thread_timeout;
	// clock_gettime(CLOCK_REALTIME, &thread_timeout);
	// timespec_add(&thread_timeout, THREAD_TIMEOUT);
	// int thread_err = 0;
	// thread_err = pthread_timedjoin(encoder_thread, NULL, thread_timeout);
	// if (thread_err) { printf("Encoder thread did not exit cleanly!"); }
	// thread_err = 0;
	// thread_err = pthread_timedjoin(print_thread, NULL, thread_timeout);
	// if (thread_err) { printf("Print thread did not exit cleanly!"); }
	// thread_err = 0;
	// thread_err = pthread_timedjoin(d1_thread, NULL, thread_timeout);
	// if (thread_err) { printf("D1 thread did not exit cleanly!"); }
	// thread_err = 0;
	// thread_err = pthread_timedjoin(d2_thread, NULL, thread_timeout);
	// if (thread_err) { printf("D2 thread did not exit cleanly!"); }
	// thread_err = 0;
	// thread_err = pthread_timedjoin(d3_thread, NULL, thread_timeout);
	// if (thread_err) { printf("D3 thread did not exit cleanly!"); }
	// thread_err = 0;
	// thread_err = pthread_timedjoin(voltage_thread, NULL, thread_timeout);
	// if (thread_err) { printf("Voltage thread did not exit cleanly!"); }
	// thread_err = 0;
	// thread_err = pthread_timedjoin(setpoint_thread, NULL, thread_timeout);
	// if (thread_err) { printf("Setpoint thread did not exit cleanly!"); }
	
	// Join and clean up threads
	pthread_join(encoder_thread, NULL);
	pthread_join(print_thread, NULL);
	pthread_join(d1_thread, NULL);
	pthread_join(d2_thread, NULL);
	pthread_join(d3_thread, NULL);
	pthread_join(voltage_thread, NULL);
	pthread_join(setpoint_thread, NULL);
	
	// exit cleanly
	power_off_imu();
	cleanup_cape();
	set_cpu_frequency(FREQ_ONDEMAND); 
	
	return 0;
}

/*******************************************************************************
* int drive_setup()
*	
* Configure the IMU for sampling at defined rate, set up the IMU interrupt
* functions,and ensure the console is set up for printing
*******************************************************************************/
int drive_setup(){
	int err = 0;
	
	// Initial setup of setpoint
	setpoint.theta = 0;
	setpoint.phi = 0;
	setpoint.gamma = 0;
	
	// Get default imu config
	imu_config_t conf = get_default_imu_config();
	
	// Configure IMU as prescribed
	conf.dmp_sample_rate = IMU_SAMPLE_RATE;
	
	// Initialize IMU
	err = initialize_imu_dmp(&imu_data, conf);
	if (err) { printf("ERROR: Failed to initialize imu"); return -1; }
	
	// Bind the interrupt function
	err = set_imu_interrupt_func(&imu_process);
	if (err) { printf("ERROR: Failed to bind IMU interrupt"); return -1; }
	
	// If successful, let us know!
	printf("Successfully configured IMU");
	
	// Create filter arrays
	float lp_num[] = LP_NUM;
	float lp_den[] = LP_DEN;
	float hp_num[] = HP_NUM;
	float hp_den[] = HP_DEN;
	float d1_num[] = D1_NUM;
	float d1_den[] = D1_DEN;
	float d2_num[] = D2_NUM;
	float d2_den[] = D2_DEN;
	float d3_num[] = D3_NUM;
	float d3_den[] = D3_DEN;
	
	// Create filters
	lp_filter = create_dfilter(LP_ORDER, lp_num, lp_den);
	hp_filter = create_dfilter(HP_ORDER, hp_num, hp_den);
	d1_filter = create_dfilter(D1_ORDER, d1_num, d1_den);
	d2_filter = create_dfilter(D2_ORDER, d2_num, d2_den);
	d3_filter = create_dfilter(D3_ORDER, d3_num, d3_den);
	
	// Configure filters
	set_dfilter_sat_limits(d1_filter, D1_SAT_MIN, D1_SAT_MAX);
	set_dfilter_sat_limits(d2_filter, D2_SAT_MIN, D2_SAT_MAX);
	set_dfilter_gain(d1_filter, D1_GAIN);
	set_dfilter_gain(d2_filter, D2_GAIN);
	set_dfilter_gain(d3_filter, D3_GAIN);
	// No gain is applied to the lp and hp filters as this would 
	// void their complementary property.
	
	// Initialize Motors
	enable_motors();
	
	// Ensure we have clean lines for writing to console
	printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n"); //x14
	
	return 0;
}

/*******************************************************************************
* int imu_process()
*	
* Process IMU data, apply a complementary filter to an estimate of the angle
* theta, and set the motor output
*******************************************************************************/
int imu_process(){		
	// Calculate the angle of the BeagleBone using the accelerometer
	mipstate.acc = atan2(-imu_data.accel[2], imu_data.accel[1]);
	
	// Since gyro_theta is the result of integration, it requires an initial
	// condition. Check if we have "initialized" gyro_theta by setting this
	// initial condition, and if not then equate gyro_theta to acc_theta.
	if (!gyro_initialized) {
		mipstate.gyro = mipstate.acc;
		gyro_initialized = 1;
	}
	
	// Calculate the angle of the BeagleBone by integrating the gyros
	mipstate.gyro += imu_data.gyro[0]*PI/180*IMU_DT;
	
	// Apply a complementary filter to theta
	mipstate.lp = advance_dfilter(lp_filter, mipstate.acc);
	mipstate.hp = advance_dfilter(hp_filter, mipstate.gyro);
	
	// Sum the complementary filter and add the cape mounting offset
	mipstate.theta = mipstate.lp + mipstate.hp + CAPE_ANGLE;
	
	// Get motor output as sum of d1 and d3 controllers
	mipstate.motor_output_L = mipstate.d1_output + mipstate.d3_output;
	mipstate.motor_output_R = mipstate.d1_output - mipstate.d3_output;
	
	// Sanitize Motor Output L
	if (mipstate.motor_output_L > 1) {
		mipstate.motor_output_L = 1;
	} else if (mipstate.motor_output_L < -1) {
		mipstate.motor_output_L = -1;
	}
	
	// Sanitize Motor Output R
	if (mipstate.motor_output_R > 1) {
		mipstate.motor_output_R = 1;
	} else if (mipstate.motor_output_R < -1) {
		mipstate.motor_output_R = -1;
	}
	
	if (setpoint.arm_state == ARMED) {
		set_motor(MOTOR_CHANNEL_R, mipstate.motor_output_R*MOTOR_POLARITY_R);
		set_motor(MOTOR_CHANNEL_L, mipstate.motor_output_L*MOTOR_POLARITY_L);
	} else {
		//preload_dfilter(d1_filter, 0);
		//preload_dfilter(d2_filter, 0);
		//preload_dfilter(d3_filter, 0);
		set_motor_free_spin_all();
	}

	
	return 0;
}

/*******************************************************************************
* int disarm_controllers()
*	
* Disarm and zero out controllers
*******************************************************************************/
int disarm_controllers(){
	setpoint.arm_state = DISARMED;
	preload_dfilter(d1_filter, 0);
	preload_dfilter(d2_filter, 0);
	preload_dfilter(d3_filter, 0);
	return 0;
}

/*******************************************************************************
* int disarm_controllers()
*	
* Zero out controllers and reset reference values
*******************************************************************************/
int cleanup_controllers(){
	preload_dfilter(d1_filter, 0);
	preload_dfilter(d2_filter, 0);
	preload_dfilter(d3_filter, 0);
	setpoint.phi = mipstate.phi;
	setpoint.gamma = mipstate.gamma;
	return 0;
}

/*******************************************************************************
* int on_pause_released()
*	
* Manually pause operation
*******************************************************************************/
int on_pause_released(){
	if (get_state() == RUNNING) {
		set_state(PAUSED);
	} else if (get_state() == PAUSED) {
		set_state(RUNNING);
	}
	return 0;
}

/*******************************************************************************
* int on_mode_released()
*	
* Do nothing, for now
*******************************************************************************/
int on_mode_released(){
	return 0;
}

/*******************************************************************************
* void* d1_control(void* ptr)
*
* Implement the outer loop controller of the successive loop closure.
*
* This controller accepts phi, a measurement of the wheel position, and sets a
* new setpoint theta. The inner loop control then targets this theta, producing
* both balance and linear positional control.
*******************************************************************************/
void* d1_control(void* ptr){
	
	while(get_state() != EXITING) {
		
		// Don't run the controller if disarmed
		if (setpoint.arm_state == ARMED) {
			// Error between setpoint theta and actual state
			mipstate.err_theta = setpoint.theta - mipstate.theta;
			// Adjust for battery voltage
			set_dfilter_gain(d1_filter, D1_GAIN*V_NOMINAL/mipstate.v_battery);
			// March filter
			mipstate.d1_output = advance_dfilter(d1_filter, mipstate.err_theta);
		}
		
		usleep(D1_MICROS);
	}
	
	return NULL;
}

/*******************************************************************************
* void* d2_control(void* ptr)
*
* Implement the outer loop controller of the successive loop closure.
*
* This controller accepts phi, a measurement of the wheel position, and sets a
* new setpoint theta. The inner loop control then targets this theta, producing
* both balance and linear positional control.
*******************************************************************************/
void* d2_control(void* ptr){
	
	while(get_state() != EXITING) {
		
		// Don't run the controller if disarmed
		if (setpoint.arm_state == ARMED) {
			// Error between setpoint phi and actual state
			mipstate.err_phi = setpoint.phi - mipstate.phi;
			// March filter
			mipstate.d2_output = advance_dfilter(d2_filter, mipstate.err_phi);
			// Set as new setpoint theta for processing by D1
			setpoint.theta = mipstate.d2_output * D1_PRESCALE;
		}
		
		usleep(D2_MICROS);
	}
	
	return NULL;
}

/*******************************************************************************
* void* d3_control(void* ptr)
*
* Implement a rudimentary steering controller.
*
* This controller takes the wheel angles, wheel radius, and track width
* to estimate gamma, the steering angle of the MiP
*******************************************************************************/
void* d3_control(void* ptr){
	
	while(get_state() != EXITING) {
		
		mipstate.gamma = (mipstate.wheelL - mipstate.wheelR)
			*WHEEL_RADIUS/(2*TRACK_WIDTH);
		
		// Don't run the controller if disarmed
		if (setpoint.arm_state == ARMED) {
			// Error estimate
			mipstate.err_gamma = setpoint.gamma - mipstate.gamma;
			// March filter with error
			mipstate.d3_output = advance_dfilter(d3_filter, mipstate.err_gamma);
		}
		
		usleep(D3_MICROS);
	}
	
	return NULL;
}

/*******************************************************************************
* void* read_encoders(void* ptr)
*
* Read the current value of the encoders and translate into individual wheel
* rotation angles wheelR and wheelL as well as average angle phi
*******************************************************************************/
void* read_encoders(void* ptr){
	
	encoder_t enabled_encoders = ACTIVE_ENCODERS;

	while(get_state() != EXITING) {
		
		mipstate.wheelL = get_encoder_pos(ENCODER_L);
		mipstate.wheelR = get_encoder_pos(ENCODER_R);
		
		
		// Check which encoders are enabled.
		// This allows for use of just one if the other is damaged,
		// since the headers break very easily.
		// By setting wheelL = wheelR, this inherently disables any
		// rotation-based steering controllers
		if (enabled_encoders == LEFT) {
			mipstate.wheelR = mipstate.wheelL;
		} else if (enabled_encoders == RIGHT) {
			mipstate.wheelL = mipstate.wheelR;
		}
		
		mipstate.wheelL *= (2*PI)/(60*GEAR_RATIO)*ENCODER_POLARITY_L;
		mipstate.wheelR *= (2*PI)/(60*GEAR_RATIO)*ENCODER_POLARITY_R;
	
		mipstate.phi = (mipstate.wheelL + mipstate.wheelR)/2 + mipstate.theta;
	
		// Encoders should update at least as fast as
		// the fastest controller!
		usleep(D1_MICROS); 
	}
	return NULL;
}

/*******************************************************************************
* void* voltage_monitor(void* ptr)
*
* Monitor the battery voltage and save to the mipstate
*******************************************************************************/
void* voltage_monitor(void* ptr){
	
	float v_new;
	
	while(get_state() != EXITING) {
		
		v_new = get_battery_voltage();
		
		// This check for nominal voltage MIGHT prevent sudden noise spikes that
		// lead to unpredictable behavior (sudden jerks, turns) in the MiP
		if ((v_new < V_MAX) && (v_new > V_MIN)) {
			mipstate.v_battery = v_new;
		} else {
			mipstate.v_battery = V_NOMINAL;
		}
		
		usleep(V_MICROS);
	}
	
	return NULL;
}

/*******************************************************************************
* void* manage_setpoint(void* ptr)
*
* Manage the setpoint and arm state
*******************************************************************************/
void* manage_setpoint(void* ptr){
	
	while(get_state() != EXITING) {
		
		if (get_state() == RUNNING) {
			static double rearm_timer = 0;
			static float filter_timeout = 0;
			
			// If the MiP falls over, disable motors to avoid damage
			if ((mipstate.theta > PI/3) || (mipstate.theta < -PI/3)){
				disarm_controllers();
				rearm_timer = 0;
			}
			
			
			// If the motors are armed and saturated, disarm after timeout
			if (setpoint.arm_state == ARMED) {
				if (dfilter_is_saturated(d1_filter)) {
					filter_timeout += D3_MICROS/1000000.0;
				} else if (dfilter_is_saturated(d2_filter)) {
					filter_timeout += D3_MICROS/1000000.0;
				} else {
					filter_timeout = 0;
				}
				if ((filter_timeout > D2_SAT_TIMEOUT) || 
						(filter_timeout > D1_SAT_TIMEOUT)) {
					rearm_timer = 0;
					filter_timeout = 0;
					disarm_controllers();
				}
			}
			
			// Rearm timer. This is reset every time a disarming condition
			// occurs, preventing the controller from re-arming until good
			// conditions are met. It also only increases when un-paused
			if (get_state() == RUNNING) {
				rearm_timer += D3_MICROS/1000000.0;
			}
			
			// Attempt to re-arm after sufficient time with no disruptions
			if ((setpoint.arm_state == DISARMED) && (rearm_timer > REARM_TIME)) {
				cleanup_controllers();
				setpoint.arm_state = ARMED;
			}
		}
		
		usleep(D3_MICROS); //can be updated infrequently
	}
	
	return NULL;
	
}

/*******************************************************************************
* void* print_data(void* ptr){ 
*	
* Print numerous properties of the MiP's performance
* May not work on non-BASH terminals
*******************************************************************************/
void* print_data(void* ptr){ 
	
	while(get_state() != EXITING){
		// Print IMU data and flush buffer
		if (verbose_output) {
			printf("\033[12A"); // Go up 12 lines
			printf("\033[K"); 	// Clear the line
			if (get_state() == RUNNING) {
				printf("State: Running\n");
			} else if (get_state() == PAUSED) {
				printf("State: Paused\n");
			} else {
				printf("State: Undefined!\n");
			}
			printf("\033[K"); 	// Clear the line
			printf("Acc Theta: %5.3f [rad]\n", mipstate.acc); //Print acc theta
			printf("\033[K"); 	// Clear the line
			printf("Gyro Theta: %5.3f [rad]\n", mipstate.gyro); //Print gyro theta
			printf("\033[K"); 	// Clear the line
			printf("LP Theta: %5.3f [rad]\n", mipstate.lp); //Print lp theta
			printf("\033[K"); 	// Clear the line
			printf("HP Theta: %5.3f [rad]\n", mipstate.hp); //Print hp theta
			printf("\033[K"); 	// Clear the line
			printf("Sum Theta: %5.3f [rad]\n", mipstate.theta); //Print sum theta
			printf("\033[K"); 	// Clear the line
			printf("Error Theta: %+-5.3f [rad]\n", mipstate.err_theta); //Error in theta
			printf("\033[K"); 	// Clear the line
			printf("Error Phi: %+-5.3f [rad]\n", mipstate.err_phi); //Error in theta
			printf("\033[K"); 	// Clear the line
			printf("Phi %+-5.3f [rad]\n", mipstate.phi); //Phi
			printf("\033[K"); 	// Clear the line
			printf("D1 Output: %+-5.3f [pcnt]\n", mipstate.d1_output); //d1_output
			printf("\033[K"); 	// Clear the line
			printf("D2 Output: %+-5.3f [pcnt]\n", mipstate.d2_output); //d2_output
			printf("\033[K"); 	// Clear the line
			printf("Motor Output: %+-5.3f [pcnt]\n", mipstate.motor_output_L); //Motor output
			printf("\033[K"); 	// Clear the line
			printf("X: %5.1f | Y: %5.1f | Z: %5.1f [m/s^2]", imu_data.accel[0], 
				imu_data.accel[1], imu_data.accel[2]); //Print xyz
			printf("\r"); //Return the cursor to beginning of the line
			fflush(stdout);
		} else {
			//printf("%7.5f\t%7.5f\t%7.5f\t%7.5f\t%7.5f\n", 
				//mipstate.acc, mipstate.gyro, mipstate.lp, mipstate.hp, mipstate.theta); 
				//Print theta and gyro
		}
	
		usleep(10000);
	}
	return NULL;
}