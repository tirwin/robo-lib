# final_project

2016 Trevor Irwin

Implementation of a mobile inverted pendulum balance controller for the MAE 144 final project, as well as a report on the controller design.

Total control system consists of a successive loop closure with the inner loop controlling theta, the angle relative to the vertical, and the outer loop controlling phi, the rotation of the wheels. The inner loop controller runs at 200 Hz, and the outer loop controller runs at 20 Hz. The inner loop controller takes the form of a lead-lag*, and the outer loop controller takes the form of a lead controller. A third controller, running at 20 Hz, tracks the steering using simple proportional control.

Features implemented as suggested in class include:

* Steering control
* Saturation and tip protection
* Arm/Disarm and Pause/Unpause states
* Battery voltage correction
* Custom discrete filter and circular buffer libraries

**ALL** files for this project are located in final_project/ or in lib/. They are:

```
final_project/final_project.c
final_project/final_config.h
lib/circular_buffer.c
lib/circular_buffer.h
lib/dfilter.c
lib/dfilter.h
```

Plus various MATLAB scripts and support files in `final_project/Report`

All code was compiled against the Robotics-Cape library from roughly October 16th.