function final_project()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Clean up workspace
close all;
clear all;
clc;
% FLAGS
DEBUG_PLOTS = 0;
DEBUG = 1;
HW_OUTPUT = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% MiP Property Definitions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mb          =   .263;       % [kg] body mass
mw          =   .027;       % [kg] wheel mass
rw          =   .034;       % [m] wheel radius
Vb          =   7.4;        % [Volts] battery voltage
sb          =   .003;       % [N*m] stall torque
wf          =   1760;       % [rad/s] motor free-run (no load)
L           =   .036;       % [m] CoM to wheel axis
gr          =   35.57;      % [] Gear Ratio
k           =   sb/wf;     % [N*m/rad/s] motor torque constant
Im          =   3.6E-8;     % [kg*m^2] motor inertia??
Ib          =   .0004;      % [kg*m^2] body inertia
Iw          =   2*((mw*rw^2)/2 + gr^2*Im); % [kg*m^2] wheel inertia
g           =   9.81;       % [m/s^2] gravitational acceleration

% syms mb mw rw Vb sb wf L gr k Im Ib Iw g;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Constant Definitions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A1 = mb*rw*L;
A2 = Ib + mb*L^2;
A3 = mb*g*L;
A4 = Iw + (mb+mw)*rw^2;
B1 = 2*gr*sb;
B2 = 2*gr^2*k;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Linear Operator Definitions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms s;
L1 = A1*s^2;
L2 = A2*s^2;
L3 = A3;
L4 = B1;
L5 = B2*s;
L6 = A4*s^2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plant Definitions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% G1 Transfer Function
G1sym = (L4*(L1 + L6))/((L6 + L5)*(L3-L5-L2) + (L5-L1)^2);
[G1num, G1den] = numden(G1sym); 
G1pnum = sym2poly(G1num);
G1pden = sym2poly(G1den);
G1 = tf(G1pnum, G1pden);
G1 = minreal(G1);

% Simplify G1 output
display(G1);
display(pole(G1));

% G2 Transfer Function
G2sym = (L3 - L2 - L1)/(L1 + L6);
[G2num, G2den] = numden(G2sym);
G2pnum = sym2poly(G2num);
G2pden = sym2poly(G2den);
G2 = tf(G2pnum, G2pden);
G2 = minreal(G2);

% Simplify G2 output
display(G2);
display(pole(G2));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% D1 Design
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create D1
k1 = -4.4;
D1Lead = tf(poly(-5.1799), poly(-25));
D1Lag = tf(poly(-5.1799), poly(0));
D1 = k1*D1Lag*D1Lead;
T1 = D1*G1/(1+D1*G1);
T1 = minreal(T1);

% P1
P1 = 1/2.28;

% D1 z
h1 = 1/200;
D1z = c2d(D1, h1, 'tustin');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% D2 Design
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% D2 Controller
k2 = 0.4;
d2lead = tf(poly(-.5), poly(-9.3486));
D2 = k2*d2lead;
display(D2);
D2 = minreal(D2);

T2 = D2*G2/(1+D2*G2);
T2 = minreal(T2);

% D2z
h2 = 1/200;
D2z = c2d(D2, h2, 'tustin');

% D2z20
h20 = 1/20;
D2z20 = c2d(D2, h20, 'tustin');

% P2
P2 = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Closed Loop
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


T3 = D2*P1*T1*G2/(1+D2*P1*T1*G2);
T3 = minreal(T3);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plotting
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (HW_OUTPUT)
    % Part 1.1
    clc;
    fprintf('\nPart 1.1:\n');
    display(G1);
    
    % Part 1.2
    fprintf('\nPart 1.2:\n');
    figure('name', 'G1 Tiny Impulse Plot', 'numbertitle', 'off');
    t = [0:.001:2];
    u = zeros(1,length(t));
    u(1) = .001;
    lsim(G1, u, t);
    axis([0, 1.5, -pi/2, .1]);
    title('MIP Response to Tiny Impulse at t = 0');
    shg;
    
    % Part 1.3
    fprintf('\nPart 1.3:\n');
    display(D1);
    
    % Part 1.4
    fprintf('\nPart 1.4:\n');
    figure('name', 'D1*G1 Bode Plot', 'numbertitle', 'off');
    bode(G1*D1);
    margin(G1*D1);
    
    % Part 1.5
    fprintf('\nPart 1.5:\n');
    display(D1z);
    
    % Part 2.1
    fprintf('\nPart 2.1:\n');
    display(G2);
    display(D2);
    
    % Part 2.2
    fprintf('\nPart 2.2:\n');
    figure('name', 'D2*G2 RLocus', 'numbertitle', 'off');
    rlocus(G2*D2);
    title('Root Locus of Open Loop G2*D2');
    
    % Part 2.3
    fprintf('\nPart 2.3:\n');
    figure('name', 'Outer Loop Step', 'numbertitle', 'off');
    step(T2);
    title('Step Response with Inner Loop Gain 1');
    
    % Part 2.4
    fprintf('\nPart 2.4:\n');
    figure('name', 'Total Loop Step', 'numbertitle', 'off');
    step(T3);
    title('Step Response of MiP System');
    
    % Part 2.5
    fprintf('\nPart 2.5:\n');
    display(D2z20);
    
    % Part 2.6
    fprintf('\nPart 2.6:\n');
    keyboard;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Debugging Plots
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% G1 Plots
if (DEBUG_PLOTS)
    figure('Name', 'G1 Bode Plot', 'numbertitle', 'off');
    bode(G1);
    figure('Name', 'G1 PZMap', 'numbertitle', 'off'); 
    pzmap(G1);
    figure('Name', 'G1 RLocus', 'numbertitle', 'off');
    rlocus(G1);
end
% G2
if DEBUG_PLOTS
    figure('name', 'G2 Bode Plot', 'numbertitle', 'off');
    bode(G2);
    figure('name', 'G2 PZMap', 'numbertitle', 'off');
    pzmap(G2);
end
% D1
if DEBUG_PLOTS
    figure('name', 'D1*G1 Rlocs', 'numbertitle', 'off');
    rlocus(D1*G1);
    figure('name', 'T1 PZMap', 'numbertitle', 'off');
    pzmap(T1);
    pole(T1);
    figure('name', 'D1 Closed Loop Step', 'numbertitle', 'off');
    step(T1);
    axis([0, 5, -1, 4]);
end
% D2
if DEBUG_PLOTS
    figure('name', 'T2 PZMap', 'numbertitle', 'off');
    pzmap(T2);
    pole(T2);
    figure('name', 'D2 Closed Loop Step', 'numbertitle', 'off');
    step(T2);
end
% T
if DEBUG_PLOTS
    figure('name', 'Closed Loop Step T', 'numbertitle', 'off');
    step(T3);
    axis([0, 15, -1, 4]);
end
% Tiny Input
if DEBUG_PLOTS
    figure('name', 'G1 Tiny Input Plot', 'numbertitle', 'off');
    axis([0, 0.5, -pi/2, 0.1]);
    t = [0:.001:2];
    u = zeros(1,length(t));
    u(1) = .001;
    lsim(G1, u, t);
    axis mode manual;
    axis([0, 1.5, -pi/2, .1]);
    title('MIP Response to Tiny Input at t = 0');
    shg;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% General Debugging
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if DEBUG
    clc;
    display(D1z);
    display(D2z);
    figure('name', 'Bode G1*D1', 'numbertitle', 'off');
    bode(G1*D1);
    margin(G1*D1);
    figure('name', 'Bode G2*D2', 'numbertitle', 'off');
    bode(G2*D2);
    margin(G2*D1);
    figure('name', 'Closed Loop Step T3', 'numbertitle', 'off');
    step(T3, 4);
    num1 = D1z.Numerator{1};
    den1 = D1z.Denominator{1};
    num2 = D2z.Numerator{1};
    den2 = D2z.Denominator{1};
    num3 = D2z20.Numerator{1};
    den3 = D2z20.Denominator{1};
    fprintf('%1.4f, %1.4f, %1.4f', num1);
    fprintf('\n');
    fprintf('%1.4f, %1.4f, %1.4f', den1);
    fprintf('\n');
    fprintf('%1.4f, %1.4f, %1.4f', num2);
    fprintf('\n');
    fprintf('%1.4f, %1.4f, %1.4f', den2);
    fprintf('\n');
    fprintf('20:\n');
    fprintf('%1.4f, %1.4f, %1.4f', num3);
    fprintf('\n');
    fprintf('%1.4f, %1.4f, %1.4f', den3);
    fprintf('\n');
    fprintf('Prefactor: %1.4f\n', P1);


    keyboard;
end