%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hw6_part3.m
% Trevor Irwin
%
% Creates a first order low-pass and first order high-pass filter.
% Converts to discrete time with Tustin's approximation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clean up workspace
close all;
clear all;
clc;

% Rise time selection
%
% Rise time for a first order filter is tr = ln(9)*tau;
% Therefore tau = tr/ln(9);
%
% The Mip takes less than .5 sec to fall over. In other words, the largest
% possible sudden motion will occur within .5 sec. If we select this as the
% rise time, we get tau = 0.23 and Wc = 4.39. This should still be a slow
% enough cut-off to catch most of the high frequency noise from the
% accelerometer. It will also hopefully be fast enough to remove the low
% frequency component of the gyros.
tr = 1.5;
tau = tr/log(9);
wc = 1/tau;
fprintf('Rise Time: %4.4f\nTau: %4.4f\nWc: %4.4f or 1/%4.2f\n', tr, tau, ...
    wc, tau);
%%
% Low & high pass filter coefficients
lpnum = [0, 1];
lpden = [tau, 1];
hpnum = [tau, 0];
hpden = [tau, 1];

% Create filters
lp = tf(lpnum, lpden);
hp = tf(hpnum, hpden);
d3 = zpk([-0.01], [-5], 1);
d3 = tf(d3);

% Convert to discrete time
dt = 0.005;
lpz = c2d(lp, dt, 'tustin');
hpz = c2d(hp, dt, 'tustin');
hd3 = 1/20;
d3z = c2d(d3, hd3, 'tustin');

% Display coefficients
display(lpz);
display(hpz);

% Plot
% fig1 = figure();
% bodeplot(lpz ,'r');
% hold on;
% bodeplot(hpz, 'g');
% h = bodeplot(lpz+hpz, 'b');
% setoptions(h, 'MagUnits', 'abs'); 
% shg;

legend('Low Pass', 'High Pass', 'Sum');

clc;
num1 = lpz.Numerator{1};
den1 = lpz.Denominator{1};
num2 = hpz.Numerator{1};
den2 = hpz.Denominator{1};
num3 = d3z.Numerator{1};
den3 = d3z.Denominator{1};
fprintf('%1.4f, %1.4f', num1);
fprintf('\n');
fprintf('%1.4f, %1.4f', den1);
fprintf('\n');
fprintf('%1.4f, %1.4f', num2);
fprintf('\n');
fprintf('%1.4f, %1.4f', den2);
fprintf('\nD3z:\n');
fprintf('%1.4f, %1.4f', num3);
fprintf('\n');
fprintf('%1.4f, %1.4f', den3);
fprintf('\n');