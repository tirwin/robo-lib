/*******************************************************************************
* final_config.h
* Trevor Irwin 2016
*
* Header containing configuration data for the MiP Balance project
*******************************************************************************/

#ifndef FINAL_CONFIG_H
#define FINAL_CONFIG_H

// IMU
#define	IMU_SAMPLE_RATE 		200		// Sample rate of IMU in Hz
#define	IMU_DT					0.005	// IMU time step

// MiP Physical Properties
#define	GEAR_RATIO				35.57	// Gear ratio from motor shaft to wheel
#define	CAPE_ANGLE				0.4		// Mounting angle of the cape
#define WHEEL_RADIUS			0.034   // Wheel radius in meters
#define TRACK_WIDTH				0.035	// Distance between the wheels
#define V_NOMINAL				7.4		// Nominal battery voltage
#define V_MICROS				100000  // Battery check period in microseconds
#define V_MAX					9 		// Maximum reasonable battery voltage
#define V_MIN					6.0		// Minimum reasonable battery voltage			

// Filters
#define	LP_ORDER				1
#define LP_NUM					{0.0036, 0.0036}
#define LP_DEN					{1.0000, -0.9927}

#define HP_ORDER				1
#define HP_NUM					{0.9964, -0.9964}
#define HP_DEN					{1.0000, -0.9927}

#define	D1_ORDER				2
#define D1_NUM					{-4.2491, 8.2810, -4.0346}
#define D1_DEN 					{1.0000, -1.8824, 0.8824}

#define D1_PRESCALE				0.4386
#define D1_GAIN					1.0
#define D1_MICROS				5000
#define D1_SAT_MIN				-1
#define D1_SAT_MAX				1
#define D1_SAT_TIMEOUT			1
#define D1_PRIORITY				1 // Thread priority (subtracted from max)

#define D2_ORDER				1
#define D2_NUM					{0.3283, -0.3202}
#define D2_DEN					{1.0000, -0.6211}

#define D2_GAIN					1.0
#define D2_MICROS				50000
#define D2_SAT_MIN				-5
#define D2_SAT_MAX				5
#define D2_SAT_TIMEOUT			2
#define D2_PRIORITY				2 // Thread priority (subtracted from max)

#define D3_ORDER				0
#define D3_NUM					{1};
#define D3_DEN					{1};

#define D3_GAIN					.25
#define D3_MICROS				50000
#define D3_PRIORITY				2 // Thread priority (subtracted from max)

#define MOTOR_CHANNEL_R			1
#define MOTOR_CHANNEL_L			4
#define MOTOR_POLARITY_R		-1
#define MOTOR_POLARITY_L		1

#define ENCODER_L				3
#define ENCODER_R				2
#define ENCODER_POLARITY_L		1
#define ENCODER_POLARITY_R		-1
#define ACTIVE_ENCODERS			BOTH	// Allows for use of single encoder

#define THREAD_TIMEOUT			2
#define REARM_TIME				1

/*******************************************************************************
* encoder_t
*
* LEFT, RIGHT, or BOTH designates which encoders will be used in calculations
* involving the encoders, such as the estimation of phi.
*
* This designation is useful in the event that one of the encoders is damaged
* or disabled for some reason.
* (The headers on the EduMiP encoders break particularly easily.)
*******************************************************************************/
typedef enum encoder_t
{
	LEFT,
	RIGHT,
	BOTH
} encoder_t;

/*******************************************************************************
* arm_state_t
*
* ARMED or DISARMED designate the current status of the controller, versus the 
* overall status of the robot (RUNNING, PAUSED, EXITING).
*
* A DISARMED controller will allow some estimation processes to continue, but
* will not allow the motors to turn and will reset the setpoint when re-armed.
*******************************************************************************/
typedef enum arm_state_t{
	ARMED,
	DISARMED
}arm_state_t;
	



#endif /*  FINAL_CONFIG_H */ 